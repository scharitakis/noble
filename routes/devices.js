var express = require('express');
var router = express.Router();
var wait=require('wait.for');

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });

    wait.launchFiber(function(request,result){
        var scanner = request.scanner;
        var deviceIds = scanner.getDevicesIds()
        for(var i=0;i<deviceIds.length;i++)
        {
            scanner.devices[deviceIds[i]].connect(function(err,res){
                result.json(res);
            })
        }



    }, req, res);
});

module.exports = router;
