

var test = function(socket){
    //TODO check scanner and emit data
    socket.emit('news',
        socket.scanner.getDevices()

    );


    socket.on('my other event', function (data) {
        console.log(data);
    });

    //Problem with the below each socket creates an event
    // When refreshing the browser the event is not removed Thus fires multiple times

    /*socket.scanner.on('started',function(data){
        socket.emit('bleStatus', { status: true });
        console.log(data +"started");
    })

    socket.scanner.on('stopped',function(data){
        socket.emit('bleStatus', { status: false });
        console.log(socket.id)
        console.log(data +"stopped");
    })


    socket.scanner.on('discover',function(data){
        socket.emit('discover', data);
        console.log(data);
    })*/

    socket.on('scanner', function (data) {
        var scanner = socket.scanner;

        if(data.status){
            scanner.startScanning(function(res){

            })
            console.log("ble started")
            console.log(data);
        }else{
            scanner.stopScanning(function(res){

            })
        }
    });


    //Connect to all devices
    socket.on('devices', function (data) {
        var deviceIds = socket.scanner.getDevicesIds();

        for(var i=0;i<deviceIds.length;i++)
        {
            socket.scanner.devices[deviceIds[i]].connect()
        }
        //socket.emit('devices', deviceIds);

    });

    //Connect to a devices
    socket.on('deviceConnect', function (data) {
        if(socket.scanner.devices[data.deviceId]){
            socket.scanner.devices[data.deviceId].connect()
        }

    });

    //Discover Services for a Device
    socket.on('discoverServices', function (data) {
        if(socket.scanner.devices[data.deviceId]){
            socket.scanner.devices[data.deviceId].discoverServices(data.deviceId);
        }

    });

    //Discover Characteristics for a Service
    socket.on('discoverCharacteristics', function (data) {
        if(socket.scanner.devices[data.deviceId]){
            socket.scanner.devices[data.deviceId].discoverCharacteristicsForServiceId(data.serviceId);
        }

    });


    //Write to Characteristics
    socket.on('writeCharacteristics', function (data) {
        if(socket.scanner.devices[data.deviceId]){
            socket.scanner.devices[data.deviceId].writeToCharacteristic(data.charId,data.data);
        }

    });

    //Write to Characteristics
    socket.on('readCharacteristics', function (data) {
        if(socket.scanner.devices[data.deviceId]){
            socket.scanner.devices[data.deviceId].readFromCharacteristic(data.charId);
        }

    });


    //Write to Characteristics
    socket.on('addNotification', function (data) {
        if(socket.scanner.devices[data.deviceId]){
            socket.scanner.devices[data.deviceId].addNotification(data.charId);
        }

    });





}


module.exports = test