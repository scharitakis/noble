var levelup = require('levelup');
var MessageController = require('./messageController');


//This is done inorder to gather all the messsages in one place rather sending the messages to the the scanner
var storage = function(params){
    this.dbPath = params.dbPath;
    this.dbOptions = params.dbOptions; //this is ={}
    this.db = levelup(this.dbPath,this.dbOptions)
    this.messageController = MessageController;


     this.db.on('put',function(key, value){
         this.messageController.emit("Storage",{type:"put",value:{key:key,value:value}})
     }.bind(this)) //emitted when a new value is 'put'

    this.db.on('del',function(key, value){
        this.messageController.emit("Storage",{type:"del",value:{key:key}})
    }.bind(this));

    this.db.on('batch',function(key, value){
        this.messageController.emit("Storage",{type:"batch",value:ary})
    }.bind(this));

    this.db.on('ready',function(key, value){
        this.messageController.emit("Storage",{type:"status",value:true})
    }.bind(this));

    this.db.on('closed',function(key, value){
        this.messageController.emit("Storage",{type:"status",value:false})
    }.bind(this));

    this.db.on('opening',function(key, value){
        this.messageController.emit("Storage",{type:"status",value:"opening"})
    }.bind(this));

    this.db.on('closing',function(key, value){
        this.messageController.emit("Storage",{type:"status",value:"closing"})
    }.bind(this));

    /* LevelUP emits events when the callbacks to the corresponding methods are called.
     db.emit('put', key, value) emitted when a new value is 'put'
     db.emit('del', key) emitted when a value is deleted
     db.emit('batch', ary) emitted when a batch operation has executed
     db.emit('ready') emitted when the database has opened ('open' is synonym)
     db.emit('closed') emitted when the database has closed
     db.emit('opening') emitted when the database is opening
     db.emit('closing') emitted when the database is closing
     */

}

storage.prototype.put=function(key,value,clb){
    this.db.put(key,value,clb);
}

storage.prototype.get=function(key){
    this.db.get(key,clb);
}

storage.prototype.del=function(key){
    this.db.del(key,clb);
}
storage.prototype.batch=function(array){
    /*
     [
     { type: 'del', key: 'father' }
     , { type: 'put', key: 'name', value: 'Yuri Irsenovich Kim' }
     , { type: 'put', key: 'dob', value: '16 February 1941' }
     , { type: 'put', key: 'spouse', value: 'Kim Young-sook' }
     , { type: 'put', key: 'occupation', value: 'Clown' }
     ]
     */
    this.db.batch(array,clb);
}


module.exports = storage