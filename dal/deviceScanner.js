var util = require('util');
var EventEmitter = require("events").EventEmitter;
var Peripheral = require('./peripheral');
var MessageController = require('./messageController');

var DeviceScanner = function(options){
    this.messageController = MessageController;
    this.peripheralsUids = (options && options.peripheralsUids)?options.peripheralsUids : null;
    this.allowDuplicates = (options && options.allowDuplicates)?options.allowDuplicates : false;
    this.noble = require('noble');
    this.devices = {};
    this.state = "";

    //======  noble Events
    this.noble.on('stateChange', this.onStateChange.bind(this));
    this.noble.on('scanStart', this.onStartScanning.bind(this));
    this.noble.on('scanStop', this.onStopScanning.bind(this));
    this.noble.on('discover', this.onDiscover.bind(this));

    this.on("deviceDisconnected",function(uuid){
        //need to remove device
        delete this.devices[uuid];
    });

    this.scanningStarted = false;
}

util.inherits(DeviceScanner,EventEmitter);

DeviceScanner.prototype.startScanning = function(/*clb*/){
    if(this.state==="poweredOn") {
        if (!this.scanningStarted){
            //this.noble.startScanning(); // any service UUID, no duplicates);
            this.noble.startScanning([], true);
            /*this.on("discover", function (data) {
                return clb(data);
            })*/
            //clb("Starting Scanner");
        }else{
            this.messageController.emit('Scanner',{type:"scanning",value:"true"})
            //return clb("Started Allready");
        }
    }
}

DeviceScanner.prototype.stopScanning = function(){
    if(this.state==="poweredOn") {
        this.noble.stopScanning();
    }
}


DeviceScanner.prototype.getDevicesIds = function()
{
    return Object.keys(this.devices);
}

DeviceScanner.prototype.getDevices = function()
{
    var devices = [];
    var deviceKeys = Object.keys(this.devices)

    for(var i=0;i<deviceKeys.length;i++){
        var device = this.devices[deviceKeys[i]];
        var pObj = {
            uuid: device.uuid,
            localname:device.advertisement.localName,
            connected:device.connected
        }
        devices.push(pObj);
    }

    return devices;

}

// ====================== Events ===========================

//Adapter state change
DeviceScanner.prototype.onStateChange = function(state){
    console.log(state)
    this.state = state;
    if(this.state!="poweredOn") {
        this.noble.stopScanning(); //Stop Scanning if not poweredOn
    }
    //state = <"unknown" | "resetting" | "unsupported" | "unauthorized" | "poweredOff" | "poweredOn">
    //this.emit('stateChange',state);
    this.messageController.emit('Scanner',{type:"state",value:state})
}

DeviceScanner.prototype.onStartScanning = function(){
    console.log("Scanning started")
    this.scanningStarted = true;
    this.messageController.emit('Scanner',{type:"scanning",value:"true"})
}

DeviceScanner.prototype.onStopScanning = function(){
    console.log("Scanner stopped");
    this.scanningStarted = false;
    this.messageController.emit('Scanner',{type:"scanning",value:"false"})
}

DeviceScanner.prototype.onDiscover = function(peripheral){
    console.log("Peripheral Discovered")
    var pObj = {
        uuid: peripheral.uuid,
        localname:peripheral.advertisement.localName
    }

    //bdf8e1c353ea4bdc9cbae39c04141402

    //dbbed7bc4dd448b29c9e229eb58e7b65

    if (this.peripheralsUids.indexOf(peripheral.uuid)!=-1) {
        //Check if this peripheral already exists
        if(this.devices[peripheral.uuid]==undefined){
            console.log("Peripheral Created =========")
            this.devices[peripheral.uuid] = new Peripheral(this,peripheral);
            this.messageController.emit('Scanner',{type:"discover",value:pObj})
            return;
        }else{
            console.log("Peripheral Already created =========")
            return;
        }
    }
    console.log(peripheral);
    //this.messageController.emit('Scanner',{type:"discover",value:pObj})

}




module.exports = DeviceScanner;