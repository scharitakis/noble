var util = require('util');
var EventEmitter = require("events").EventEmitter;



//This is done inorder to gather all the messsages in one place rather sending the messages to the the scanner
var messageController = function(){
    console.log('============This is the message Controller')
}

messageController.instance = null;

messageController.getInstance = function(){
    if(this.instance === null){
        this.instance = new messageController();
    }
    return this.instance;
}

util.inherits(messageController,EventEmitter);

module.exports = messageController.getInstance();