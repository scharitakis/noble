var util = require('util');
var EventEmitter = require("events").EventEmitter;
var async = require('async');
var MessageController = require('./messageController');
var say = require('say')


var Peripheral = function(deviceScanner,peripheral){
    this.messageController = MessageController;
    this.deviceScanner = deviceScanner;
    this.uuid = peripheral.uuid;
    this.peripheral = peripheral;
    this.advertisement = peripheral.advertisement;
    this.localName = this.advertisement.localName;
    this.txPowerLevel = this.advertisement.txPowerLevel;
    this.manufacturerData = this.advertisement.manufacturerData;
    this.serviceData = this.advertisement.serviceData;
    this.serviceUuids = this.advertisement.serviceUuids;

    this.peripheral.on('connect', this.onConnect.bind(this));
    this.peripheral.on('disconnect', this.onDisconnect.bind(this));
    this.peripheral.on('rssiUpdate', this.onRssiUpdate.bind(this));
    this.peripheral.on('servicesDiscover', this.onServicesDiscover.bind(this));

    this.servicesIds = []; //[serviceids]
    this.services = null; //{serviceUid:service}
    this.serviceCharacteristics = {}; //{serviceUid:[characteristicsids]}
    this.characteristics = {}; //{characteristicsId : characteristicObj}
    this.characteristicDescriptors = {} // {characteristicsId : [descriptorId]}
    this.descriptors = {}; //{descriptorId:descriptorObj}
    this.notifications = []; //[characteristics]
    this.connected = false;

}

util.inherits(Peripheral,EventEmitter);

//Connect and find Services
Peripheral.prototype.connect = function(/*clb*/){

    if(!this.connected) {
        this.peripheral.connect(function (error) {
            if (error) {
                this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"error",msg:"Could not Connect to Peripheral"}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
                return;
               }
            this.connected = true;
            this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"connect",value:true}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
        }.bind(this)) //This is one way of adding this to the callback
    }else{
        this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"connect",value:true}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
    }
}

Peripheral.prototype.disconnect = function(/*clb*/){
    if(this.connected) {
        this.peripheral.disconnect(function (error) {
            if (error) {
                this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"error",msg:"Could not Disconnect from Peripheral"}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
                return;
            }
            this.connected = false;
            this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"connect",value:false}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
        }.bind(this))
    }else{
        this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"connect",value:false}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
    }
}


Peripheral.prototype.discoverServices = function(serviceUids/*,clb*/){
    if(this.servicesIds.length)
    {
        this.messageController.emit("Peripheral", {
            puuid: this.peripheral.uuid,
            type: "discoverServices",
            value: this.servicesIds
        });
    }else {
        this.peripheral.discoverServices([], function (error, services) {
            if (error) {
                this.messageController.emit("Peripheral", {
                    puuid: this.peripheral.uuid,
                    type: "error",
                    msg: "Could not discover Services"
                }); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
                return;
            }
            var servicesObj = {}
            for (var i = 0; i < services.length; i++) {
                var serviceUuid = services[i].uuid;
                servicesObj[serviceUuid] = services[i];
                this.servicesIds.push(serviceUuid);
            }
            this.services = servicesObj;

            /*this.discoverCharacteristicsForServiceId(this.servicesIds[0],function(err,data){
             console.log(data);
             })*/

            /*this.discoverCharacteristicsForAllServices(function(err,data){
             console.log("discoverCharacteristicsForAllServices");
             console.log(data);
             console.log("+++++++++++++++++++++++++++");
             })*/

            this.messageController.emit("Peripheral", {
                puuid: this.peripheral.uuid,
                type: "discoverServices",
                value: this.servicesIds
            }); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK

        }.bind(this))
    }
}

Peripheral.prototype.discoverCharacteristicsForServiceId = function(serviceId/*,clb*/)
{
    //var _this = this;
    var service = this.services[serviceId];
    if(service) {
        service.discoverCharacteristics([], function (error, characteristicsArray) {
            if (error) {
                //_this.emit("error", "discoverCharacteristics")
                //return clb(error, null);
                this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"error",msg:"Could not discover characteristics"}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK

            }
            var characteristicsuids = [];
            for (var i = 0; i < characteristicsArray.length; i++) {
                this.characteristics[characteristicsArray[i].uuid] = characteristicsArray[i];
                characteristicsuids.push(characteristicsArray[i].uuid)
            }
            this.serviceCharacteristics[serviceId] = characteristicsuids;
            this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"discoverCharacteristics",value:characteristicsuids}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK
            //return clb(error, characteristicsuids);
        }.bind(this))
    }else{
        this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"error",msg:"No Service was found"}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK

        //return clb("error", "no service found");
    }
}

//discover The characteristics for all Services (a Service can have many Characteristics )
Peripheral.prototype.discoverCharacteristicsForAllServices = function(clb){
    if(this.servicesIds.length !=0) {
        var _this = this;
        var serviceIndex = 0;
        var characteristicsIds = {}
        async.whilst(
            function () {
                return (serviceIndex < _this.servicesIds.length);
            },
            function (callback) {

                _this.services[_this.servicesIds[serviceIndex]].discoverCharacteristics([], function (error, characteristicsArray) {
                    if (error) {
                        this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"error",msg:"Could not discover Characteristics"}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK

                        _this.emit("error", "discoverCharacteristics");
                        return callback(error);
                    }
                    var characteristicsuids = [];
                    for (var i = 0; i < characteristicsArray.length; i++) {
                        characteristicsIds[characteristicsArray[i].uuid] = characteristicsArray[i];
                        characteristicsuids.push(characteristicsArray[i].uuid)
                    }
                    _this.serviceCharacteristics[_this.servicesIds[serviceIndex]] = characteristicsuids;//characteristics;

                    serviceIndex++;
                    return callback()

                })

            },
            function (error) {
                _this.characteristics = characteristicsIds;
                /*_this.discoverDescriptors(function(err,data){
                    console.log(data);
                });*/


                // ++++++++++++++++++ The below can be removed
                _this.readFromAllCharacteristics(function(err,data){
                    console.log(data);
                });

                _this.writeToCharacteristic('a495ff23c5b14b44b5121370f02d74de',"stavros",function(err,data){
                    console.log("clb write to ========")
                    console.log(err)
                    console.log(data)
                })
                // ++++++++++++++++++ The below can be removed END

                this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"characteristics",value:Object.keys(_this.characteristics)}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK

                //return clb(null, Object.keys(_this.characteristics));
            }
        );
    }else{
        //return clb("error","No services found");
        this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"error",msg:"No services found"});

    }
}

Peripheral.prototype.getAllCharacteristicsIds = function(){
    return Object.keys(this.characteristics);
}

Peripheral.prototype.readFromAllCharacteristics = function(clb){
    var _this = this;
    var characteristicIds = this.getAllCharacteristicsIds();
    var characteristicIndex = 0;
    var dataRes = {}
    async.whilst(function(){
        return (characteristicIndex < characteristicIds.length);
    },
    function(callback){
        _this.readFromCharacteristic(characteristicIds[characteristicIndex],function(error,data){
            if (error) {
                _this.emit("error", "reading data for characteristics")
                return callback(error);
            }

            dataRes[characteristicIds[characteristicIndex]] = data;
            characteristicIndex++;
            return callback()

        })
    },
    function(error)
    {
        return clb(null,dataRes);
    })
}

Peripheral.prototype.readFromCharacteristic = function(characteristicId/*,clb*/){
    var characteristic = this.characteristics[characteristicId];
    if (characteristic.properties.indexOf('read') !== -1) {
        characteristic.read(function(error, data) {
            if (data) {
                var string = data.toString('ascii');
                console.log('\n    value       ' + data.toString('hex') + ' | \'' + string + '\'');
                //return clb(null,string);

                this.messageController.emit("Peripheral",{
                    puuid:this.peripheral.uuid,
                    cuuid:characteristicId,
                    type:"readCharacteristic",
                    value:string
                });
                return;
            }else{
                this.messageController.emit("Peripheral",{
                    puuid:this.peripheral.uuid,type:"error",
                    msg:"Could not Read Characteristics"
                });
                return;
                //return clb("error","no data");
            }
        }.bind(this));
    } else {
        this.messageController.emit("Peripheral",{
            puuid:this.peripheral.uuid,
            type:"error",
            msg:"Cannot read for this characteristic"
        });
        //return clb(null,"cannot read for this characteristic");
    }

}

Peripheral.prototype.toHex = function(str) {
    var result = '';
    for (var i=0; i<str.length; i++) {
        result += str.charCodeAt(i).toString(16);
    }
    return result;
}



Peripheral.prototype.writeToCharacteristic = function(characteristicId,data/*,clb*/){
    var characteristic = this.characteristics[characteristicId];
    if (characteristic.properties.indexOf('write') !== -1) {
        var dataBuffer = new Buffer(data);
        characteristic.write(dataBuffer,false,function(error) {//TODO : check notify flag
            if(error)
            {
                this.messageController.emit("Peripheral",{
                    puuid:this.peripheral.uuid,
                    cuuid:characteristicId,
                    type:"error",
                    msg:"Cannot write to this characteristic"
                });
                return;
                //return clb(error,null);
            }else{
                this.messageController.emit("Peripheral",{
                    puuid:this.peripheral.uuid,
                    cuuid:characteristicId,
                    type:"writeCharacteristic",
                    value:true}); // THIS IS A TEST TO SEE IF CASCADING EVENT WORK


                //return clb(null,"Done writing");
            }

        }.bind(this));
    }
    else {
        this.messageController.emit("Peripheral",{
            puuid:this.peripheral.uuid,
            cuuid:characteristicId,
            type:"error",
            msg:"characteristic is not writable"
        });
        //return clb(null,"cannot write for this characteristic");
    }

}

Peripheral.prototype.discoverDescriptors = function(clb){
    var _this = this;
    var characteristicsUids = this.getAllCharacteristicsIds();

    var characteristicIndex = 0;

    async.whilst(
        function () {
            return (characteristicIndex < characteristicsUids.length);
        },
        function(callback) {
            var characteristic = _this.characteristics[characteristicsUids[characteristicIndex]];

            characteristic.discoverDescriptors(function(error, descriptorsArray) {
                var descriptorsIds =[]
                for(var i=0;i<descriptorsArray.length;i++){
                    var descriptor = descriptorsArray[i];
                    descriptorsIds.push(descriptor.uuid);
                    _this.descriptors[characteristicsUids[characteristicIndex]+":"+descriptor.uuid]  = descriptor;
                }

                _this.characteristicDescriptors[characteristicsUids[characteristicIndex]] = descriptorsIds
                characteristicIndex ++
                return callback();

            })

            /*async.series([
                function(callback)
                {
                    characteristic.discoverDescriptors(function(error, descriptors) {
                        async.detect(
                            descriptors,
                            function(descriptor, callback) {
                                return callback(descriptor.uuid === '2901'); // it will return false if descriptor.uuid == 2901 Thus it will not stop
                            },
                            function(userDescriptionDescriptor){
                                if (userDescriptionDescriptor) {
                                    userDescriptionDescriptor.readValue(function(error, data) {
                                        if (data) {
                                            console.log('(####' + data.toString() + '####)');
                                        }
                                        callback();
                                    });
                                } else {
                                    callback();
                                }
                            }
                        );

                    });
                },
                function(callback)
                {
                    console.log('\n    properties For characteristic  ' + characteristic.uuid);
                    console.log('\n    properties  ' + characteristic.properties.join(', '));

                    if (characteristic.properties.indexOf('read') !== -1) {
                        characteristic.read(function(error, data) {
                            if (data) {
                                var string = data.toString('ascii');

                                cconsole.log('\n    value       ' + data.toString('hex') + ' | \'' + string + '\'');
                            }
                            callback();
                        });
                    } else {
                        callback();
                    }
                },
                function() {
                    characteristicIndex++;
                    callback();
                }
            ]);*/



        },
        function(error){
            return clb(null, Object.keys(_this.descriptors));
        })

}


Peripheral.prototype.updateRssi = function(clb){
    this.peripheral.updateRssi(function(error, rssi){
        if(error){
            this.emit("error","updateRssi") // most probably you can get the info from event
            return clb(error,null);
        }
        return clb(null,rssi);
    }.bind(this));

}


//--------------------------------------------------------------

Peripheral.prototype.addNotification = function(characteristicId/*,clb*/){
    var characteristic = this.characteristics[characteristicId];
    if(this.notifications.indexOf(characteristicId) == -1) {
        // Event that is fired when enabling notfication
        characteristic.on('notify',function(state){
            //console.log("aaaaaaaaaaaa")
            this.messageController.emit("Peripheral",{
                puuid:this.peripheral.uuid,
                cuuid:characteristicId,
                type:"notify"
                ,value:state});

        }.bind(this));

        //Event when there is a notification
        characteristic.on('read',function(data,notification){
            say.speak('Victoria', 'Intruder Alert, Intruder Alert, get out of my house');
            var string = data.toString('ascii');
            console.log(string)
            this.messageController.emit("Peripheral",{
                puuid:this.peripheral.uuid,
                cuuid:characteristicId,
                type:"notifyRead",
                value:string});

        }.bind(this));

        //Enable notification
        characteristic.notify(true, function (err) {
            if(err){
                //this.emit("notificationError","Could not add Notification")

                this.messageController.emit("Peripheral",{
                    puuid:this.peripheral.uuid,
                    cuuid:characteristicId,
                    type:"error",
                    msg:"Could not add Notification"});
                return;

            }else{
                //this.emit("notification",characteristicId);
                this.notifications.push(characteristicId);
                //return clb(null,"Notifications enabled for characteristics: "+characteristicId)
            }
        }.bind(this));

    }else{
        this.messageController.emit("Peripheral",{
            puuid:this.peripheral.uuid,
            cuuid:characteristicId,
            type:"error",
            msg:"Notifications already enabled for characteristic: "+characteristicId});

        //return clb("error","Notifications already enabled for characteristics")
    }
}

Peripheral.prototype.removeNotification = function(characteristic){
    var index = this.notifications.indexOf(characteristic.uuid)
    if(index!= -1) {
        characteristic.notify(false,function(err){
            if(err){
                //this.emit("notificationError","Could not remove Notification")
                this.messageController.emit("Peripheral",{
                    puuid:this.peripheral.uuid,
                    cuuid:characteristicId,
                    type:"error",
                    msg:"Notifications already enabled for characteristic: "+characteristic.uuid});
            }else{
                //this.emit("notification",false);
                this.notifications.splice(index,1);
            }
        })
    }
}


// ----------------  Events --------------------

Peripheral.prototype.onConnect = function(){
    this.connected = true;
    this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"connect",value:true});

    // this.emit("connect",true);
}

Peripheral.prototype.onDisconnect = function(){
    this.connected = false;
    //this.emit("disconnect",true);
    this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"connect",value:false});
    this.deviceScanner.emit("deviceDisconnected",this.peripheral.uuid)


}

Peripheral.prototype.onRssiUpdate = function(rssi){
    //this.emit("rssiUpdate",rssi);
    this.messageController.emit("Peripheral",{puuid:this.peripheral.uuid,type:"rssiUpdate",value:rssi});

}

Peripheral.prototype.onServicesDiscover = function(services){
    this.emit("servicesDiscover",services);
}


module.exports = Peripheral;