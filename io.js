var testio = require('./routesio/test');
var deviceScanner = require('./dal/deviceScanner')
var MessageController = require('./dal/messageController');
var Storage = require('./dal/storage');
var config = require('./config/config');

var socketio = function(io){

    var storage = new Storage(config.db);
    var scanner = new deviceScanner({peripheralsUids :['dbbed7bc4dd448b29c9e229eb58e7b65','3e847d95a13341658964ca03c33c2075'],allowDuplicates :true});

    console.log("socket times");

    io.use(function(socket, next){
        socket.scanner = scanner;
        socket.storage = storage;
        console.log("socket.io midleware")
        if (socket.request.headers.cookie) return next();
        next(new Error('Authentication error'));
    });
    //scanner.on("started")

    io.sockets.on('connection', function (socket) {
        //socket.scanner()
        testio(socket);

    });


    //Peripheral and Scanner Events

    MessageController.on('Scanner',function(data) {
        io.sockets.emit('Scanner', data);
        console.log('messageController Scanner');
    })

    MessageController.on('Peripheral',function(data) {
        io.sockets.emit('Peripheral', data);
        console.log('messageController Peripheral');
    })

    /*scanner.on('perror',function(data){
        try{
            io.sockets.emit('devices', data);
            console.log(data +"peripherals error===========");
        }catch(e){
            console.log(e)
        }

    })


    scanner.on('pConnected',function(data){
        try{
            io.sockets.emit('devices', data);
            console.log(data +"peripherals connect===========");
        }catch(e){
            console.log(e)
        }

    })


    scanner.on('discoverServices',function(data){
        try{
            io.sockets.emit('devices', data);
            console.log(data +"peripherals services===========");
        }catch(e){
            console.log(e)
        }

    })


    /// Scanner events

    scanner.on('started',function(data){
        try{
            io.sockets.emit('bleStatus', { status: true });
            console.log(data +"started");
        }catch(e){
            console.log(e)
        }

    })

    scanner.on('stopped',function(data){
        io.sockets.emit('bleStatus', { status: false });
        console.log(data +"stopped");
    })


    scanner.on('discover',function(data){
        io.sockets.emit('discover', data);
        console.log(data);
    })
*/
}

module.exports = socketio;