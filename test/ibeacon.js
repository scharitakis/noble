var noble = require('noble');

//console.log(noble)

noble.on('scanStart', function(){
    console.log("scan has started")
});

noble.on('stateChange',function(state){
    console.log(state);
});

function startScanning(){
    //noble.startScanning(["<service b03b373edda34e408df824b64e7a1d85>"],false);
    noble.startScanning([],false);
}

var EXPECTED_MANUFACTURER_DATA_LENGTH = 25;
var APPLE_COMPANY_IDENTIFIER = 0x004c; // https://www.bluetooth.org/en-us/specification/assigned-numbers/company-identifiers
var IBEACON_TYPE = 0x02;
var EXPECTED_IBEACON_DATA_LENGTH = 0x15;

var d1 = [];

function OnDiscover(peripheral){
        console.log(noble)
        var manufacturerData = peripheral.advertisement.manufacturerData;
        var rssi = peripheral.rssi;

        console.log('onDiscover: manufacturerData = %s, rssi = %d', manufacturerData ?  manufacturerData.toString('hex') : null, rssi);

        if (manufacturerData &&
            EXPECTED_MANUFACTURER_DATA_LENGTH <= manufacturerData.length &&
            APPLE_COMPANY_IDENTIFIER === manufacturerData.readUInt16LE(0) &&
            IBEACON_TYPE === manufacturerData.readUInt8(2) &&
            EXPECTED_IBEACON_DATA_LENGTH === manufacturerData.readUInt8(3)) {

            var uuid = manufacturerData.slice(4, 20).toString('hex');
            var major = manufacturerData.readUInt16BE(20);
            var minor = manufacturerData.readUInt16BE(22);
            var measuredPower = manufacturerData.readInt8(24);

            console.log('onDiscover: uuid = %s, major = %d, minor = %d, measuredPower = %d', uuid, major, minor, measuredPower);

        }

        var accuracy = Math.pow(12.0, 1.5 * ((rssi / measuredPower) - 1));
        var proximity = null;

        if (accuracy < 0) {
            proximity = 'unknown';
        } else if (accuracy < 0.5) {
            proximity = 'immediate';
        } else if (accuracy < 4.0) {
            proximity = 'near';
        } else {
            proximity = 'far';
        }

        /*peripheral.connect(function(err){
            console.log("connected")
            console.log(err)
            setInterval(function(){
                var manufacturerData = peripheral.advertisement.manufacturerData;
                var measuredPower1 = manufacturerData.readInt8(24);
                var rssi1 = peripheral.rssi;
                console.log(measuredPower1+"-"+rssi1)
            },3600)

        })*/

        /*peripheral.connect(function(err) {
            console.log("connected")
            console.log(err)
            d1.push(peripheral);
        });*/

    peripheral.connect(function(err){
        console.log("peripheral connected")
        //console.log(peripheral)
    })

    peripheral.on('disconnect', function(){
        console.log("peripheral disconnected")
        d1.length=0;
        noble.stopScanning();
        d1.length=0;
        noble.startScanning([],false);
    });
    d1.push(peripheral);

        console.log(proximity)
        //noble.removeListener('discover', OnDiscover);
        //noble.stopScanning();
        //noble.addListener('discover', OnDiscover);
        //startScanning();
        //console.log(peripheral);
}

noble.on('discover',OnDiscover);

setInterval(function(){
    if(d1.length) {
        var peripheral = d1[0];
        var rssi = peripheral.rssi;
        var manufacturerData = peripheral.advertisement.manufacturerData;
        var measuredPower = manufacturerData.readInt8(24);
        var accuracy = Math.pow(12.0, 1.5 * ((rssi / measuredPower) - 1));
        var proximity = null;

        if (accuracy < 0) {
            proximity = 'unknown';
        } else if (accuracy < 0.5) {
            proximity = 'immediate';
        } else if (accuracy < 4.0) {
            proximity = 'near';
        } else {
            proximity = 'far';
        }

        console.log(proximity);
    }
},1200)

startScanning();
