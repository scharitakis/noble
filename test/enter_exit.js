/*
 Continously scans for peripherals and prints out message when they enter/exit
 In range criteria:      RSSI < threshold
 Out of range criteria:  lastSeen > grace period
 based on code provided by: Mattias Ask (http://www.dittlof.com)
 */
var noble = require('noble');

var RSSI_THRESHOLD    = -90;
var EXIT_GRACE_PERIOD = 2000; // milliseconds

var inRange = {};

var obj = function(params){
    this.peri =params.peri;
    this.uuid = params.uuid;
    this.lastSeen = params.lastSeen;
}


noble.on('discover', function(peripheral) {

    console.log('===========discovered===========');
    var uuid = peripheral.uuid;
    if(inRange[uuid]==undefined) { // do not create an object if already exist
        var peri = new obj({peri: peripheral, uuid: peripheral.uuid, lastSeen: Date.now()})
        inRange[uuid] = peri
    }else{
        inRange[uuid].lastSeen =Date.now();
    }

});

setInterval(function() {
    //console.log(inRange);
        for (var uuid in inRange) {
            var d = Date.now() - EXIT_GRACE_PERIOD
            if (inRange[uuid].lastSeen < d) {

                var peripheral = inRange[uuid].peripheral;

                //console.log('"' + peripheral.advertisement.localName + '" exited (RSSI ' + peripheral.rssi + ') ' + new Date());

                delete inRange[uuid];
            }else {
                var peripheral = inRange[uuid].peri;
                var rssi = peripheral.rssi;

                var manufacturerData = peripheral.advertisement.manufacturerData;
                var measuredPower = manufacturerData.readInt8(24);
                var accuracy = Math.pow(12.0, 1.5 * ((rssi / measuredPower) - 1));
                var proximity = null;

                console.log(rssi +"||"+measuredPower)

                if (accuracy < 0) {
                    proximity = 'unknown';
                } else if (accuracy < 0.5) {
                    proximity = 'immediate';
                } else if (accuracy < 4.0) {
                    proximity = 'near';
                } else {
                    proximity = 'far';
                }

                console.log(proximity +" uuid:"+uuid);
            }
        }


}, EXIT_GRACE_PERIOD / 2);

noble.startScanning([], true);