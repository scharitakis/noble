/**
 * Created by sce9sc on 3/14/15.
 */
var socket = io();


//=================== Receiving =======================

socket.on('news', function (data)
{
    console.log(data);
    socket.emit('my other event', { my: 'data1' });
});


//Test for messageController
socket.on('Scanner', function (data)
{
    document.getElementById('console').innerHTML = JSON.stringify(data);
    console.log(data);
    var type = data.type;
    switch(type){
        case "error":
            break;
        case "scanning":
            break;
        case "state":
            break;
        case "discover":
            addDiscoverDevice(data);
            break;
    }

});

//Test for messageController
socket.on('Peripheral', function (data)
{
    document.getElementById('console').innerHTML = JSON.stringify(data);
    console.log(data);
    var type = data.type;
    switch(type){
        case "error":
            break;
        case "connect":
            updateConnectedDeviceList(data);
            break;
        case "discoverServices":
            addDiscoverServices(data)
            break;
        case "discoverCharacteristics":
            addDiscoverCharacteristics(data)
            break;
        case "readCharacteristic":
            break;
        case "writeCharacteristic":
            break;
        case "rssiUpdate":
            break;
        case "notify":
            break;  // fired when enabling notification
        case "notifyRead":
            break; //fired when an enabled notification has changed value
    }

});


//=================== Sending =======================

function scannning(params){
    console.log("Scanning : "+params)
    socket.emit('scanner', { status: params });
}

//connect to all discovered Devices
function devices(params){
    socket.emit('devices', { status: params });
}

function characteristics(service){
    socket.emit('characteristics', { service: service });
}

function deviceConnect(){
    var deviceId = getSelectedDevices()
    socket.emit('deviceConnect', { deviceId: deviceId });
}

function discoverServices(){
    var deviceId = getSelectedDevices()
    socket.emit('discoverServices', { deviceId: deviceId });
}

function discoverCharacteristics(){
    var deviceId = getSelectedDevices()
    var serviceId = getSelectedService()
    socket.emit('discoverCharacteristics', {
        deviceId: deviceId,
        serviceId: serviceId });
}

function writeCharacteristics(){
    var deviceId = getSelectedDevices()
    var serviceId = getSelectedService()
    var charId = getSelectedCharact()
    var data = $('#txt').val();
    socket.emit('writeCharacteristics', {
        deviceId: deviceId,
        serviceId: serviceId,
        charId:charId,
        data:data
    });
}

function readCharacteristics(){
    var deviceId = getSelectedDevices()
    var serviceId = getSelectedService()
    var charId = getSelectedCharact()
    //var data = $('#txt').val();
    socket.emit('readCharacteristics', {
        deviceId: deviceId,
        serviceId: serviceId,
        charId:charId//,
        //data:data
    });
}

function addNotification(){
    var deviceId = getSelectedDevices()
    var serviceId = getSelectedService()
    var charId = getSelectedCharact()
    //var data = $('#txt').val();
    socket.emit('addNotification', {
        deviceId: deviceId,
        serviceId: serviceId,
        charId:charId//,
        //data:data
    });
}


// Manipulation


function setDevice (dev){
    var dev = $(dev);
    var selected = $('#deviceListGroup .active')
    $.each(selected,function(i,v){
        $(v).removeClass('active')
    })
    console.log(selected)
    dev.addClass('active');
}

function addDiscoverDevice(data){
    var elhtml = '<a id="'+data.value.uuid+'" href="#!" data-value="'+data.value.uuid+'" onclick="setDevice(this)" class="list-group-item">'+
        '<span class="badge"> disconnected</span>' +
        '<span>'+data.value.localname+'</span></a>'

    var el = $(elhtml)
    $('#deviceListGroup').append(el)
}

function getSelectedDevices(){
    var selected = $('#deviceListGroup .active')
    return $(selected[0]).data().value;

}

function updateConnectedDeviceList(data){
    var selected = $('#'+data.puuid +' .badge');
    if(data.value)
    {
        selected.html('connected');
    }
    else{
        selected.html('disconnected');
    }

}



//Services

function addDiscoverServices(data){
    var serviceListGroup =  $('#serviceListGroup')
    serviceListGroup.empty();
    var html = "";
    for(var i=0;i<data.value.length;i++){
        html += '<a id="'+data.value[i]+'" href="#!" data-value="'+data.value[i]+'" onclick="setService(this)" class="list-group-item">'+
            '<span class="badge"> disconnected</span>' +
            '<span>'+data.value[i]+'</span></a>'
    }


    var el = $(html)
    serviceListGroup.append(el)
}

function setService (dev){
    var dev = $(dev);
    var selected = $('#serviceListGroup .active')
    $.each(selected,function(i,v){
        $(v).removeClass('active')
    })
    console.log(selected)
    dev.addClass('active');
}

function getSelectedService(){
    var selected = $('#serviceListGroup .active')
    return $(selected[0]).data().value;

}

//characteristics

function addDiscoverCharacteristics(data){
    var charactListGroup =  $('#charactListGroup')
    charactListGroup.empty();
    var html = "";
    for(var i=0;i<data.value.length;i++){
        html += '<a id="'+data.value[i]+'" href="#!" data-value="'+data.value[i]+'" onclick="setCharact(this)" class="list-group-item">'+
        '<span class="badge"> disconnected</span>' +
        '<span>'+data.value[i]+'</span></a>'
    }


    var el = $(html)
    charactListGroup.append(el)
}

function setCharact (dev){
    var dev = $(dev);
    var selected = $('#charactListGroup .active')
    $.each(selected,function(i,v){
        $(v).removeClass('active')
    })
    console.log(selected)
    dev.addClass('active');
}

function getSelectedCharact(){
    var selected = $('#charactListGroup .active')
    return $(selected[0]).data().value;

}