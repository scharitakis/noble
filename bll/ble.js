var async = require('async');
var noble = require('noble');

var peripheralUuid = "bdf8e1c353ea4bdc9cbae39c04141402";//process.argv[2];

var peripheralUuids = ['d03972cb82f7'];
var characteristicIds = ['a495ff21c5b14b44b5121370f02d74de', //scratch1 characteristicIds
                    'a495ff22c5b14b44b5121370f02d74de', //scratch2 characteristicIds Temperature
                    'a495ff23c5b14b44b5121370f02d74de', //scratch3 characteristicIds
                    'a495ff24c5b14b44b5121370f02d74de', //scratch4 characteristicIds
                    'a495ff25c5b14b44b5121370f02d74de' //scratch5 characteristicIds
                 ]

noble.on('stateChange', function(state) {
    if (state === 'poweredOn') {
        noble.startScanning();
    } else {
        noble.stopScanning();
    }
});

noble.on('discover', function(peripheral) {
    if (peripheralUuids.indexOf(peripheral.uuid)!=-1) {
        noble.stopScanning();

        console.log('peripheral with UUID ' + peripheralUuid + ' found');
        var advertisement = peripheral.advertisement;

        var localName = advertisement.localName;
        var txPowerLevel = advertisement.txPowerLevel;
        var manufacturerData = advertisement.manufacturerData;
        var serviceData = advertisement.serviceData;
        var serviceUuids = advertisement.serviceUuids;

        if (localName) {
            console.log('  Local Name        = ' + localName);
        }

        if (txPowerLevel) {
            console.log('  TX Power Level    = ' + txPowerLevel);
        }

        if (manufacturerData) {
            console.log('  Manufacturer Data = ' + manufacturerData.toString('hex'));
        }

        if (serviceData) {
            console.log('  Service Data      = ' + serviceData);
        }

        if (localName) {
            console.log('  Service UUIDs     = ' + serviceUuids);
        }

        console.log();

        explore(peripheral);
    }
});

function explore(peripheral) {
    console.log('services and characteristics:');

    peripheral.on('disconnect', function() {
        process.exit(0);
    });

    peripheral.connect(function(error) {
        peripheral.discoverServices([], function(error, services) {
            var serviceIndex = 0;

            async.whilst(
                function () {
                    return (serviceIndex < services.length);
                },
                function(callback) {
                    var service = services[serviceIndex];
                    var serviceInfo = service.uuid;

                    if (service.name) {
                        serviceInfo += ' (---' + service.name + '---)';
                    }
                    console.log(serviceInfo);

                    service.discoverCharacteristics([], function(error, characteristics) {
                        var characteristicIndex = 0;

                        async.whilst(
                            function () {
                                return (characteristicIndex < characteristics.length);
                            },
                            function(callback) {
                                var characteristic = characteristics[characteristicIndex];
                                var characteristicInfo = '  ' + characteristic.uuid;

                                if (characteristic.name) {
                                    characteristicInfo += ' (|||' + characteristic.name + ')';
                                }
                                // find the descriptors for each Characteristic
                                async.series([
                                    function(callback) {
                                        characteristic.discoverDescriptors(function(error, descriptors) {
                                            characteristicInfo += " \n Detect Descriptors for characteristic (="+ characteristic.uuid +"=) \n";
                                            async.detect(
                                                descriptors,
                                                function(descriptor, callback) {
                                                    return callback(descriptor.uuid === '2901'); // it will return false if descriptor.uuid == 2901 Thus it will not stop
                                                },
                                                function(userDescriptionDescriptor){
                                                    if (userDescriptionDescriptor) {
                                                        userDescriptionDescriptor.readValue(function(error, data) {
                                                            if (data) {
                                                                characteristicInfo += ' (####' + data.toString() + '####)';
                                                            }
                                                            callback();
                                                        });
                                                    } else {
                                                        callback();
                                                    }
                                                }
                                            );

                                        });
                                    },
                                    function(callback) {
                                        characteristicInfo += '\n    properties For characteristic  ' + characteristic.uuid;
                                        characteristicInfo += '\n    properties  ' + characteristic.properties.join(', ');

                                        if (characteristic.properties.indexOf('read') !== -1) {
                                            characteristic.read(function(error, data) {
                                                if (data) {
                                                    var string = data.toString('ascii');

                                                    characteristicInfo += '\n    value       ' + data.toString('hex') + ' | \'' + string + '\'';
                                                }
                                                callback();
                                            });
                                        } else {
                                            callback();
                                        }
                                    },
                                    function() {
                                        console.log(characteristicInfo);
                                        characteristicIndex++;
                                        callback();
                                    }
                                ]);
                            },
                            function(error) {
                                serviceIndex++;
                                callback();
                            }
                        );
                    });
                },
                function (err) {
                    peripheral.disconnect();
                }
            );
        });
    });
}
