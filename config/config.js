var config = {
    db: {
        dbPath:'./mydb',
        dbOptions:{
            valueEncoding:'json'
        }
    }
}

module.exports = config;