//#include <Bounce2.h>

//#include <Bounce.h>

/*
  Bean Scratch Commands

  This sketch is intended to demonstrate how
  you can use the Bean's scratch characteristics
  to send commands to the onboard arduino and
  return data.  I use the LightBlue IOS app to
  write the commands to Scratch characteristic
  1 and read the returned data from the other
  scratch characteristics.

  Valid commands are:
    ON       - turn LED on
    OFF      - turn LED off
    RED      - set LED color to red
    GREEN    - set LED color to green
    BLUE     - set LED color to blue
    TEMP     - get temperature reading and output to Scratch 2 in form "Temperature: ##"

  Invalid commands flash the LED three times rapidly.

  NOTE: This sketch is not a low-power sketch.

  This example code is in the public domain.
*/
//#include <Button.h>
#define MAX_BEAN_SLEEP 0xFFFFFFFF //hex =4294967295
#define BTN_RED 0
boolean bLedOn = false;
LedReading ledColor = { 255, 0, 0 };
ScratchData scratchCommand;
ScratchData scratchBlank;
boolean Activated = false;
boolean Detected = false;
String SecurityCode = "STAVROS";
boolean SecOK = false;
int sleepTimer = 2000;
static bool connected = false;

// Create button objects
//Button btnRed = Button( BTN_RED, LOW);
//Bounce debouncer = Bounce();

void setup()
{
  //Serial.begin(57600);
  //Serial.setTimeout(5);

  Bean.enableWakeOnConnect(true);


  //change this when button
   pinMode( BTN_RED, INPUT_PULLUP);
  //pinMode(BTN_RED,INPUT);
  //btnRed.setDebounceDelay(5);
  //debouncer.attach(BTN_RED);
  //debouncer.interval(5);
  // Start with LED off
  Bean.setLed( 0, 0, 0 );

  uint8_t buffer[1] = { ' ' };

  // Initialize scratch 1 with blank space
  Bean.setScratchData( 1, buffer, 1 );
  // Clear scratch 2 for returning data
  Bean.setScratchData(2, buffer, 1 );
  // Clear scratch 3 for returning data
  Bean.setScratchData(3, buffer, 1 );

  // Flash LED to indicate setup done
  delay(10);
  Bean.setLed( 255, 255, 255 );
  delay(250);
  Bean.setLed( 0, 0, 0 );
  delay(10);

}

// ISR needs to return void and accept no arguments
void wakeBean(){}

void loop()
{

   connected = Bean.getConnectionState();

  //btnRed.listen();
  //debouncer.update();
  //int value = debouncer.read();
   Bean.attachChangeInterrupt(0,wakeBean);
   uint8_t pinValue = digitalRead(0);
  if((bool)SecOK){

    String activeCmd = getActivated();


    if(activeCmd=="ACTIVATE")
    {
      Activated = true;
    }else
    {
      Activated = false;
    }


    if (Activated)
    {
      if ((bool)pinValue)
      {
         //delay(400);
          Detected = true;

          uint8_t nTemp = Bean.getTemperature();
          delay(10);
          // Build the temperature string
          String strTemp = "Temperature: " + (String) nTemp;

          // Convert the string to a uint8_t array
          uint8_t bufTemp[20];
          for ( int i=0; i<strTemp.length(); i++ )
          {
            bufTemp[i] = strTemp.charAt(i);
          }

          // Write the temperature array to scratch 2
          Bean.setScratchData( 2, bufTemp, strTemp.length() );
          delay(10);
          // Flash LED green to indicate TEMP command was processed
          Bean.setLed( 0, 0, 0 );
          delay(10);
          Bean.setLed( 0, 255, 255 );
          delay(250);
          Bean.setLed( 0, 0, 0 );
          delay(10);
          //debouncer.update();
          delay(100);
      }
    }
  }

  if(!SecOK)
  {
        String secCode = getSecurityCode();

  // If the command is empty or if it is a single space then there is no command to process
        if ( ((secCode.length()) > 0) && (secCode!= " ") )
        {
          // Process command
            if(secCode==SecurityCode)
            {
              String secAccepted = "secOK ";
              sleepTimer = MAX_BEAN_SLEEP;
              SecOK = true;
              uint8_t secBufTemp[20];
              for ( int i=0; i<secAccepted.length(); i++ )
              {
                secBufTemp[i] = secAccepted.charAt(i);
              }
              Bean.setScratchData( 3, secBufTemp, secAccepted.length() );
              delay(10);

              Bean.setLed( 0, 255, 0 ); //GREEN
              delay(50);
              restoreLedSetting();
            }else{
              Bean.setLed( 0, 0, 255 ); //BLUE
              delay(150);
              restoreLedSetting();
            }
      }
      else
      {
        // If unknown command,
        // flash LED times RED
        Bean.setLed( 255, 0, 0 );
        delay(50);
        // Set LED to saved setting
        restoreLedSetting();
      }
  }



  // if disconnect we need a way to reset the bean and reconnect again from the beginning .
  // disconnect from controller and then open close the door in order to reset.
  if(!connected){
    uint8_t buffer[1] = { ' ' };
      // Initialize scratch 1 with blank space
    Bean.setScratchData( 1, buffer, 1 );
    // Clear scratch 2 for returning data
    Bean.setScratchData(2, buffer, 1 );
    // Clear scratch 3 for returning data
    Bean.setScratchData(3, buffer, 1 );
    sleepTimer = 2000;
    Activated = false;
    Detected = false;
    SecOK = false;
  }




  //Bean.setLed( 0, 255, 0 );
  //restoreLedSetting();


   Bean.sleep(sleepTimer);
}

String getActivated()
{
  ScratchData scratch1 = Bean.readScratchData(1);
  delay(10);
  String strCmd1 = "";
  for (int i=0; i<scratch1.length; i++)
  {
    strCmd1 += (String) (char) scratch1.data[i];
  }
  strCmd1.toUpperCase();

  return strCmd1;
}

String getSecurityCode(){
  ScratchData scratch3 = Bean.readScratchData(3);
  delay(10);
  String strCmd3 = "";
  for (int i=0; i<scratch3.length; i++)
  {
    strCmd3 += (String) (char) scratch3.data[i];
  }
  strCmd3.toUpperCase();

  return strCmd3;
}



String getCommand()
{
  // Read the command from scratch 1
  ScratchData scratchCommand = Bean.readScratchData(1);
  delay(10);
  // Convert command to a String object
  String strCmd = "";
  for (int i=0; i<scratchCommand.length; i++)
  {
    strCmd += (String) (char) scratchCommand.data[i];
  }
  strCmd.toUpperCase();

  if ( ((strCmd.length()) > 0) && (strCmd!= " ") )
  {
    // Clear the command so we don't process twice
    uint8_t buffer[1] = { ' ' };
    Bean.setScratchData( 1, buffer, 1 );
    delay(10);
  }

  return strCmd;
}

void restoreLedSetting()
{
  // This delay ensures we have at least 10ms between Bean commands
  delay(10);
  if (bLedOn)
  {
    Bean.setLed( ledColor.red, ledColor.green, ledColor.blue );
    delay(10);
  }
  else
  {
    Bean.setLed( 0, 0, 0 );
    delay(10);
  }
}